﻿using System;

namespace week_3_exercise_14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is your name?");
            var usrName = Console.ReadLine();
            Console.WriteLine($"Hello, {usrName}");
        }
    }
}
